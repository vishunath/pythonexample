from bs4 import BeautifulSoup
import re
html_doc = """
<html><head><title>The Dormouse's story</title>
<script>js script should be here</script></head>
<body>
<p class="title"><b>The Dormouse's story</b></p>
<p class="title"><b><!--The Dormouse's story--></b></p>
<p class="story">Once upon a time there were three little sisters; and their names were
<a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
<a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
<a href="http://example.com/tillie" class="brother" id="link3">Tillie</a>;
and they lived at the bottom of a well.</p>

<p class="story">...</p>
</body> </html>
"""
soupObject = BeautifulSoup(html_doc,'html.parser');

#print(soupObject.prettify())

#for the title tag we can use title keyword directly and we can do the same for the other tags also like-
# for paragraph use p and for url link use 'a' and can use b also.
# for exmaple soupObject.title, soupObject.p, soupObject.a, soupObject.b, soupObject.body etc etc.

print(soupObject.title)
print(soupObject.title.parent)
print(soupObject.title.parent.text)
print(soupObject.p)
print(soupObject.findAll('p'))

for a in soupObject.findAll('a'):
    print(a['href'],"  ", a.get('id'),"  ",a.get('class'))

print(soupObject.getText())

p_list = soupObject.findAll('p')
print(p_list[2].a)

# contents and children can be used to iterate over the children tags

for child in soupObject.contents:
    print(child)
# or using children --

for child in soupObject.children:
    print(child)

print(soupObject.body.contents)

# descendants --> attribute lets you iterate over all of a tag’s children, recursively: its direct children,
# the children of its direct children, and so on
print("descendants ------>")
for child in soupObject.body.descendants:
    print(child)

#only the text part within given tag --

print("----------------->>>------------>>")
for string in soupObject.strings:
    print(string)

# to remove the extra whicte-space from the string use stripped_strings --

print("----------------->>>>>-----------")
for string in soupObject.stripped_strings:
    print(string)

# to check the parent of any tag we can use the parent keyword -

print("parent of title tag is --- ", soupObject.title.parent.name)


# to get all the parents of a tag we can use parents keywords and it will iterate over all the tags and returns the parents from deep to top --
# for the given tag 'a' it will print p,body,html,[document].  these are the parents of the given 'a' tag directly or indirectly.
print("###############-----############")
for parent in soupObject.a.parents:
    print(parent.name)

#.next_sibling or .previous_sibling can be used to find the sibling of the tags which are on same level.
# its not working --
print("next sibling --- ", soupObject.script.next_sibling)

# next_elements to move forward in the given html context.
# print the elements after the body tag in given html

for elem in soupObject.body.next_elements:
    print(elem)

#searching all the tags starting with 't'
print(" ------using regex with findAll() method---- ")
for tag in soupObject.findAll(re.compile('t')):
    print(tag.name)

## pass a list of tags as parameter to findAll()

print(" print tags with t and b character")
for tag in soupObject.findAll(["title","b"]):
    print(tag)

## find all the tags not the string
print("  ---- print all the tags only ---")
for tag in soupObject.findAll(True):
    print(tag.name)

#  If none of the other matches work for you, define a function that takes an element as its only argument.
#  The function should return True if the argument matches, and False otherwise.
#  Pass this function into find_all() and you’ll pick up all the <p> tags:

def hasClassButNoId(tag):
    return tag.has_attr('class') and not tag.has_attr('id')

print(" ****** ---- print only p becasue it has class as attribute ------ ")
for tag in soupObject.findAll(hasClassButNoId):
    print(tag)

# uses of find_all(name, attrs, recursive, string, limit, **kwargs)
print(" ****** - find all the tags which have specific attr")

for tag in soupObject.findAll(id="link1"):
    print(tag)

print(" ****** - find all the tags which have specific string ****")

for tag in soupObject.findAll(string=re.compile("story")):
    print(tag)

print(" **** find all the tags which have specific href ****")

for tag in soupObject.findAll(href=re.compile("tillie")):
    print(tag)

print(" **** find all the tags where id has something ****")
for tag in soupObject.findAll(id=True):
    print(tag)

print(" **** find multiple attribute at the once ****")

for tag in soupObject.findAll(href=re.compile("tillie"),id="link3"):
    print(tag)

for tag in soupObject.findAll(href=re.compile("tillie"),class_="brother"):
    print(tag)

# finding css class with length of class value should be equal to given number
print("########## finding css class with length of class value should be equal to given number #####")

def has_six_characters(css_class):
    return css_class is not None and len(css_class) == 7

for tag in soupObject.findAll(class_=has_six_characters):
    print(tag)