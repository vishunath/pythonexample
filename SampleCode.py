#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 14:57:38 2018

@author: tkmah7u
"""


# printing the given word

def printTwice(word):
    print (word*4)
    
word="'hello'"
printTwice(word)

# checking the odd or even number

def ifElse(num):
    if num % 2 == 0:
        print("even")
    else:
        print("odd")
        
# calling the ifElse(num) method        
num=51
ifElse(num)

#compare(x, y) for comparing the integer

def compare(x, y):
    if x>y:
        print("x is greater than y")
    elif x < y:
        print("x is less than y")
    else :
        print("x is equal to y")
        
        
#calling the comapre function
compare(5,4)


#recursive function

def countDown(num):
    if(num==0):
        print ("blast off!!!")
    else:
        print(num)
        countDown(num-1)
        
#calling countDown function
num=10
countDown(10)


# taking input from user

#userInput = input("enter something\n")
#print(int(userInput)*2)


# checking whether given value x is between y and z. 

def isBetween(x,y,z):
    return y<x<z

print (isBetween(5,4,6))


# WRITING factorial function

def fact(num):
    if num<1:
        return 1
    else:
        result=num * fact(num-1)
        return result
        
print(fact(5))

# writing fact for integer only not for string and float

def fact2(num):
    if not isinstance(num,int):
        print("please provide the integer")
        return -1
    elif num<0 :
        print("please provide the positive integer")
        return -1
    elif num==0:
        return 1
    else:
        return num * fact(num-1)
    
#print(fact2(1.2))


# Fibonacci function

def fibonacci(num):
    if num==0 or num==1:
        return 1
    else:
        return fibonacci(num-1)+ fibonacci(num-2)
    
print(fibonacci(5))


# index finder of a given char in string

def indexFinder(string,char,index):
    finalIndex=-1
    while index <len(string):
        if string[index]==char:
            #finalIndex=index
            return index
        else:
            index = index+1
    return finalIndex

string="awregrf"
char="w"
print(indexFinder(string,char,3))

list=["a","b","c","g","t"]
print(list[1:3])
list[1:3]=[]
print(list[1:3])
print(len(list))
print(list)

# create a function for swaping number

def swap(x,y):
    return y,x

a,b=4,5
a,b=swap(a,b)
print(a,b)


# create a function forn geneating random numbers

import random
def randomNumber():
    for i in range(5,10):
        print(int(random.random()*10*i))
        
randomNumber()



def mapFunction():
    map={"name":"vishu","city":"pune"}
    print(map["name"])
    for key in map.keys():
        print(key)
    
mapFunction()