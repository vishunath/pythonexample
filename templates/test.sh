#!/usr/bin/env bash

executeTest() {
    local no_op=$1
    if [ -z "$no_op" ]; then
        no_op="true"
    fi

    local version=$2
    if [ -z "$version" ]; then
        version="1.0.0-SNAPSHOT"
        echo "Version not specified. Defaulting to version [$version]"
    fi

    local script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

    # required configs that would be sourced from the config repo
    BQUTIL_PROJECT="kohls-ddh-lle"
    {{ tableNameInCaps }}_BQUTIL_DATASET="kohls_dev_dp_lab_db"
    {{ tableNameInCaps }}_BQUTIL_TABLE="bqIngestTest_{{ tableName }}"
    {{ tableNameInCaps }}_BQUTIL_SOURCE_FORMAT="ORC"
    {{ tableNameInCaps }}_BQUTIL_SOURCE_URIS="gs://kohls-dev-dp-logistics/gold/location/hive/gold_hct_lclzn_str_clustr/*"
    {{ tableNameInCaps }}_BQUTIL_LOG_LEVEL="INFO"

    {{ tableNameInCaps }}_BQUTIL_TMP_DIR=${script_dir}/temp

    . ${script_dir}/../../target/{{ tableName }}-app-${version}/ingest-table.sh $no_op
    return $?
}
executeTest $1 $2
