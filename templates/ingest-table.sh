#!/usr/bin/env bash

ingestTable() {
    local no_op=$1

    # no_op is an optional parameter used for testing to allow execution of the script
    # up to the point of executing the bq command against BigQuery
    # valid values are true & false (default)
    if [ -z "$no_op" ]; then
        no_op="false"
    fi

    local script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

    . ${script_dir}/config/bash-env.properties

    # TODO: we will need to support daily updates.
    # we either need to code something here to figure out which partitions to load or add it to the bqutil
    # for now, we will just replace the table

    $script_dir/libs/bigQueryUtility/bigqueryutil.sh action=REPLACE_TABLE \
                                                    project_id=$BQUTIL_PROJECT \
                                                    dataset_id=${{ tableNameInCaps }}_BQUTIL_DATASET \
                                                    table_id=${{ tableNameInCaps }}_BQUTIL_TABLE \
                                                    source_format=${{ tableNameInCaps }}_BQUTIL_SOURCE_FORMAT \
                                                    source_uris="${{ tableNameInCaps }}_BQUTIL_SOURCE_URIS" \
                                                    log_level=${{ tableNameInCaps }}_BQUTIL_LOG_LEVEL \
                                                    multi_step_def="" \
                                                    temp_dir="${{ tableNameInCaps }}_BQUTIL_TMP_DIR" \
                                                    noop="$no_op" \
   #                                                 partition_type="INGEST_DATE" \
   #                                                 partition_format="YYYYMMDD"
    return $?
}

ingestTable $1
