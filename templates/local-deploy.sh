#!/bin/bash

VERSION=$1
if [ -z "$VERSION" ]; then
    VERSION=1.0.0-SNAPSHOT
fi

local-deploy() {
    local script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

    rm -r ${script_dir}/app/target/{{ tableName }}-app-$VERSION
    tar xvzf ${script_dir}/app/target/{{ tableName }}-app-$VERSION.tar.gz -C ${script_dir}/app/target/

    rm -r ${script_dir}/azkaban/target/bqingestion-{{ tableName }}
    unzip ${script_dir}/azkaban/target/azkaban-flow-$VERSION.zip -d ${script_dir}/azkaban/target/
}
local-deploy
