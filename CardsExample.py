#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 31 00:19:04 2018

@author: tkmah7u
"""
class Card:
    suitList = ["Clubs","Diamonds","Hearts","Spades"]
    rankList = ["narf", "Ace", "2", "3", "4", "5", "6", "7",
              "8", "9", "10", "Jack", "Queen", "King"]
    def __init__(self,suit,rank):
        self.suit = suit
        self.rank = rank
    
    def __str__(self):
        return self.rankList[self.rank] + " of " + self.suitList[self.suit]
    
    def __cmp__(self,card):
        
        if self.suit > card.suit : return 1
        
        if self.suit < card.suit : return -1
        
        if self.rankList[self.rank] == "Ace" and card.rankList[card.rank] == "Ace" : return 0
        
        if self.rankList[self.rank] == "Ace"  : return 1
        
        if card.rankList[card.rank] == "Ace" : return -1
        
        if self.rank > card.rank : return 1
        
        if self.rank < card.rank : return -1
        
        return 0
    


card1 = Card(3,1)
print (card1)

card2 = Card(3,13)
print(card2)

print("comparing both cards")
print (card1.__cmp__(card2) )

#####################################################

class Deck:
    #constructor
    def __init__(self):
        self.cards = []
        for suit in range(4):
            for rank in range(1,14):
                self.cards.append(Card(suit,rank))
                
# overriding the string method
                
    def __str__(self):
        s = ""
        for i in range(len(self.cards)):
            s = s + " "*i + str(self.cards[i]) + "\n"
        return s
                
 # printing the cards               
    def printDetails(self):
        self.list1 = list()
        for card in self.cards:
            self.list1.append(card.__str__())
            print (card)

# Suffling the cards
            
    def suffle(self):
        import random
        nCards = len(self.cards)
        for i in range(nCards):
            j =  random.randrange(i,nCards)
            self.cards[i],self.cards[j] = self.cards[j],self.cards[i]
 
## removing the card from the deck
            
    def removeCard(self,card):
        
        if card in self.list1:
            print(" value of card "+card)
            self.list1.remove(card)
            return True
        else:
            return False
            

deck = Deck()
print(deck.printDetails())


deck.suffle()
print(deck.printDetails())

print (deck.removeCard('2 of Clubs'))
#print(deck.printDetails())

#print (deck)
#print(deck.list1)