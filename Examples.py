
def reverse(string):
    print(string[-1::-1])

def nonMain():
    print("hello-world!!!")

def main():
    print("hello")
    nonMain()
    reverse("hello")

if __name__ == '__main__':
    main()
