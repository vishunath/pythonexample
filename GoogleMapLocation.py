import json
import urllib.request,urllib.parse,urllib.error

restApiForMap = 'http://maps.googleapis.com/maps/api/geocode/json?'

location = "Pune"

fullUrl = restApiForMap  + urllib.parse.urlencode({'address':location})

RestResponse = urllib.request.urlopen(fullUrl)

jsonResponse = RestResponse.read().decode()

jsonParser = json.loads(jsonResponse)

#print(jsonParser)
if len(jsonParser["results"]) >= 1:
    print("lattitude : ",jsonParser["results"][0]["geometry"]["location"]["lat"])
    print("longitude : ",jsonParser["results"][0]["geometry"]["location"]["lng"])
else:
    print(jsonParser)
