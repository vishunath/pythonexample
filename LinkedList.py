#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 15:03:36 2018

@author: tkmah7u
"""

class Node:
    
    def __init__(self,cargo=None,next=None):
        self.cargo = cargo
        self.next = next
        
    def __str__(self):
        return self.cargo
    
    def traverse(node):
        while node:
            print (node.cargo)
            node = node.next
            
    def printBackWard(self,node):
        if node == None:
            return 
        head = node
        list = node.next
        self.printBackWard(self,list)
        print (head)
        
    def removeNode(self,node,cargo):
        self.head = node
        if node == None:
            return None
        if node.cargo == cargo:
            return node.next
        
        self.prevNode = node
        node = node.next
        
        while node:
            print("node ???", node.cargo)
            print("prevNode ??",self.prevNode.cargo)
            if node.cargo == cargo:
                self.prevNode.next = node.next
                node.next = None
                return self.head
                
            self.prevNode = node
            node = node.next

                
        
                
                
        
        


# create a node

firstNode = Node("node1")
secondNode = Node("node2")
thirdNode = Node("node3")
fourthNode = Node("node4")


firstNode.next = secondNode
secondNode.next = thirdNode
thirdNode.next = fourthNode
fourthNode.next = None

#print(firstNode.cargo + " ---> "+ firstNode.next.cargo)
#print(secondNode.cargo + " ---> "+ secondNode.next.cargo)
#print(thirdNode.cargo + " ---> "+ thirdNode.next.cargo)
#print(fourthNode.cargo + " ---> " + str(fourthNode.next))


#calling the traverse method

Node.traverse(firstNode)

print("reverse traversing ")
Node.printBackWard(Node,firstNode)

listNode = Node.removeNode(Node,firstNode,"node1")

print("<----------after removing node --------->")
Node.traverse(listNode)