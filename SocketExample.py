
# create socket and make a get call to retrieve data from server
import socket
import urllib.request, urllib.parse,urllib.error

mySocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

mySocket.connect(('data.pr4e.org',80))

command = 'GET http://data.pr4e.org/words.txt HTTP/1.1\n\n'.encode()
#command = ''
mySocket.send(command)

print(mySocket)
mySocket.sendall("GET / HTTP/1.1\r\nHost: data.pr4e.org\r\n\r\n".encode())

while True :
    data = mySocket.recv(512)
    if len(data) < 1:
        break
    print(data.decode())
mySocket.close()

fhand = urllib.request.urlopen('http://data.pr4e.org/romeo.txt')
for line in fhand:
    print(line.decode())