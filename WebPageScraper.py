from bs4 import BeautifulSoup
import urllib.request, urllib.parse,urllib.error

url = 'http://www.dr-chuck.com'
urlResponse = urllib.request.urlopen(url).read()

print(urlResponse)
soupObject = BeautifulSoup(urlResponse,'html.parser')

tags = soupObject('a')

for tag in tags:
    print(tag.get('href',None))