import xml.etree.ElementTree as ET

xmlFile = open("/Users/tkmahdw/tkmah7u/pythonexample/xmlFile.xml")

xmlData = xmlFile.read()
#print(xmlData)

#start reading the data from xml

xmlString = '''<parent>
        <groupId>com.kohls.bigdata.selfserve.bigqueryingestion</groupId>
        <artifactId>bigdata-selfserve-bigqueryingestion</artifactId>
        <version>1.0.0-SNAPSHOT</version>
        <relativePath>../pom.xml</relativePath>
    </parent>'''
tree = ET.parse("/Users/tkmahdw/tkmah7u/pythonexample/templates/sample.xml")

root = tree.getroot()

#for childs in root:
#    print(childs)
#    print(childs.text)


lists = tree.iterfind('modules/module');
for list in lists:
    print("module : "+list.text, "attribute=",list.get("name"))
