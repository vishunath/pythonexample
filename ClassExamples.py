#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 25 01:10:52 2018

@author: tkmah7u
"""

    # creating a class and accessing its member
    
# __init__ function is like constructure but it is not. It is used to assign the variables. Although we can add 
#  any extra varible/member function at run time, we don't have to defined in init always.
#  variables defined outside the __init__() is treated as class varible or static variable.
 

class Point:
    def __init__(self,x,y):
        self.x = x
        self.y = y
        
    z=2 #static variable
    
    def equal(self,point):
        return (self.x==point.x and self.y==point.y)
        
        
        
p1=Point(1,2)

print(p1.x)


print(p1.__dir__())
print(Point.z)
print(p1.z)

# comparing two object using shallow equality

p2=Point(1,2)
p3=Point(1,2)

print(p2==p3)  #it will result fasle bcz both p2 and p3 are pointing to different object

# comparing two object using deep equality
 
print(p2.equal(p3))


# creating a modifer function (a function which modify the existing object)

class Time:
    def __init__(self,hour,minute,second):
        self.hour = hour
        self.minute = minute
        self.second = second
        
def increment(time, seconds):
    
    time.second = time.second + seconds
    
    if time.second >= 60:
        noOfMinutes = int(time.second / 60)
        time.second = time.second % 60
        time.minute = time.minute + noOfMinutes
        
    if time.minute >= 60 :
        noOfHours = int(time.minute / 60)
        time.minute = time.minute % 60
        time.hour = time.hour + noOfHours
        

# creating a time cobject and calling increment function to add seconds to the time.

currentTime = Time(12,58,55)
increment(currentTime,125)
print (currentTime.hour,":",currentTime.minute,":",currentTime.second)

        

