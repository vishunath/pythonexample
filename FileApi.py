#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 23 15:36:09 2018

@author: tkmah7u
"""
# creating function to read file from file-system

def readFile(fileUri):
    f = open(fileUri,"r")
    for line in f:
        print(line)
    
    f.close()
    
readFile("/Users/tkmafaf/tkmah7u/gcp-files/edge_nodes.text")

# creating a function for writing file/copying a file

def copyFile(sourceUri,targetUri):
    srcFile = open(sourceUri,"r")
    destFile = open(targetUri,"w")
    content = srcFile.read()
    destFile.write(content)
    destFile.close()
    srcFile.close()
    
copyFile("/Users/tkmafaf/tkmah7u/gcp-files/edge_nodes.text",
         "/Users/tkmafaf/tkmah7u/gcp-files/backup_edge_nodes.text")


# creating a function to write a file which contains/preserve data-structure

def preserveDS(filePath):
    import pickle
    f = open(filePath,"wb")
    pickle.dump(123,f)
    pickle.dump(1234.65,f)
    f.close()
    
preserveDS("/Users/tkmafaf/tkmah7u/gcp-files/dsType.pck")
    

# creating a function to read data from pickle file or(serialized file)

def readPicklData(filePath):
    import pickle
    f = open(filePath,"rb")
    intData = pickle.load(f)
    print(intData,type(intData))
    floatData = pickle.load(f)
    print(floatData,type(floatData))

readPicklData("/Users/tkmafaf/tkmah7u/gcp-files/dsType.pck")


