#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  5 17:03:40 2018

@author: tkmah7u
"""
from jinja2 import Environment, FileSystemLoader

def xmlEditor(xmlPath):
    file = open(xmlPath,"r")
    for line in file:
        if "<groupId>" in line and "gold-[a-z][\\-]" in line:
            print (line)
        if "<artifactId>" in line:
            print (line)
    

#xmlEditor("/Users/tkmafaf/tkmah7u/pythonExamples/xmlFile.xml")


def listFiles():
    import glob
    import re
    dirName = "/Users/tkmahdw/tkmah7u/bigdata-selfserve-bigquery-ingestion/gold_hcth_sls_trn_sku_agg"
    tableName = "gold_hcth_sls_trn_sku_agg"
    for list in glob.iglob("/Users/tkmahdw/tkmah7u/bigdata-selfserve-bigquery-ingestion/gold_hcth_sls_trn_sku_agg/**/*",recursive=True):
        
        if "ingest-table.sh" in list:
            print (list)
        if "local-deploy.sh" in list:
            print (list)
        if "([a-z])*.sh" in list:
            print (list)
        if "app/pom.xml" in list:
            print (list)
        if "azkaban/pom.xml" in list:
            print (list)
        if dirName+"/pom.xml" in list:
            print(list)
        if re.search("test_[a-z_]*\.sh",list):
            print(list)
            print(re.findall("test_[a-z_]*\.sh",list))

    
        
listFiles()

def template():
    env = Environment(loader=FileSystemLoader("/Users/tkmafaf/tkmah7u/pythonExamples/templates"),trim_blocks=True)
    #print (env.get_template("module.xml").render(tableName='customTable'))
    #print (env.get_template("app.xml").render(tableName='customTable'))
    #print (env.get_template("azkaban.xml").render(tableName='customTable'))
    print (env.get_template("test.sh").render(tableName='customTable',tableNameInCaps='CUSTOM_TABLE'))
    print (env.get_template("local-deploy.sh").render(tableName='customTable',tableNameInCaps='CUSTOM_TABLE'))
    print (env.get_template("ingest-table.sh").render(tableName='customTable',tableNameInCaps='CUSTOM_TABLE'))
    
    

#template() 